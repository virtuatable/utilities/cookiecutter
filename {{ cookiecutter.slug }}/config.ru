# frozen_string_literal: true

require 'bundler'
Bundler.require :runtime

Virtuatable::Application.load!('{{ cookiecutter.slug }}')

run Controllers::{{ cookiecutter.resource }}
